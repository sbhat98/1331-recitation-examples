# 1331 Recitation Examples

## Running this on your computer

To get this repository on your computer, go to your terminal and `cd` into the directory you wish to keep the repository in. Then run

```sh
git clone https://gitlab.com/sbhat98/1331-recitation-examples
```

Whenever you would like to update your code to get changes, `cd` into the repository directory and run

```sh
git pull
```

## Getting different versions

To get versions of files from previous recitations, you can "checkout" past commits in the repository.

```sh
#Listing all the tags
git tag -l

#To change to a specific tag
git checkout [tagname]
```

Example:

```sh
git checkout rec4
```

To go back to the current version:

```sh
git checkout master
```
