public class Mammal extends Animal {
	
	public void growHair() {
		System.out.println("Growing hair...");
	}

	public Mammal(String s) {
		super(s);
	}

	private void helperMethod() {
		super.eat();
	}

	public void eat() {
		System.out.println("Nothing");
	}

}