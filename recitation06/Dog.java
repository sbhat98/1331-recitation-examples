public class Dog extends Mammal {
	
	public void sound() {
		System.out.println("Woof!");
	}

	public Dog() {
		this("Dog");
	}

	public Dog(String s) {
		super(s);
	}


}