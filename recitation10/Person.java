public class Person implements Comparable<Person> {

	private String name;
	private int age;

	public Person(int x, String name) {
		age = x;
		this.name = name;
	}

	@Override
	public int compareTo(Person other) {
		return this.age - other.age;
	}

	// @Override
	// public int compareTo(Person other) {
	// 	return this.name.compareTo(other.name);
	// }

	@Override
	public String toString() {
		return "" + age + "Name: " + name;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

}