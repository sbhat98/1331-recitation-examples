import java.util.*;

public class Main {
	public static void main(String[] args) {
		List<Person> list = new ArrayList<>();

		list.add(new Person(2, "Bob"));
		list.add(new Person(4, "Alice"));
		list.add(new Person(1, "Eve"));

		System.out.println(list);

		var comparator = new PersonNameComparator();

		Collections.sort(list, comparator);

		System.out.println(list);
			
	}
}