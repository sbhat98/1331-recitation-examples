import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Recitation14 extends Application {

    private ObservableList<String> todos;
    int maxSize;

    @Override public void start(Stage stage) {
        ObservableList<String> todos = FXCollections.observableArrayList();
        ListView<String> listView = new ListView<String>(todos);

        Button addButton = new Button();
        addButton.setText("Add");
        Label l = new Label("curr size: 0");
        Label x = new Label("max size: 0");

        TextField inputField = new TextField();

        addButton.setOnAction(e -> {
        	if (!inputField.getText().isEmpty()) {
        		todos.add(inputField.getText());
        		l.setText("curr size: " + todos.size());
        		if (todos.size() > maxSize) {
        			maxSize = todos.size();
        			x.setText("max size: " + maxSize);
        		}
        	}
            inputField.setText("");
            inputField.requestFocus();
        });

        // addButton.disableProperty()
        //         .bind(Bindings.isEmpty(inputField.textProperty()));


        HBox entryBox = new HBox(inputField, addButton);
        // entryBox.getChildren().addAll(inputField, addButton);

        VBox vbox = new VBox(l, x, listView, entryBox);
        // vbox.getChildren().addAll(listView, entryBox);

        Scene scene = new Scene(vbox);
        stage.setScene(scene);
        stage.setTitle("Todos");
        stage.show();
    }
}
