public class LinkedList<T> {
	
	class Node {
		Node(T data, Node next) {
			this.data = data;
			this.next = next;
		}

		T data;
		Node next;
	}

	private Node head;

	public void addToFront(T data) {
		if (head == null) {
			head = new Node(data, null);
		} else {
			Node prevHead = head;
			head = new Node(data, prevHead);
		}
	}

	public int size() {
		return sizeHelper2(head, 0);
	}

	private int sizeHelper(Node currNode) {
		if (currNode == null) {
			return 0;
		} else {
			return 1 + sizeHelper(currNode.next);
		}
	}

	private int sizeHelper2(Node currNode, int counter) {
		if (currNode == null) {
			return counter;
		} else {
			return sizeHelper2(currNode.next, counter + 1);
		}
	}





	public boolean contains(T data) {
		return containsHelper(data, head);
	}

	private boolean containsHelper(T data, Node currNode) {
		if (currNode == null) {
			return false;
		} else if (data.equals(currNode.data)) {
			return true;
		} else {
			return containsHelper(data, currNode.next);
		}
	}
}