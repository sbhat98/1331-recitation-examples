public class Dog {

	private String breed;
	private int age;
	private String name;


	public Dog(String breed, String name, int age) {
		this.breed = breed;
		this.name = name;
		this.age = age;
	}

	public String getBreed() {
		return breed;
	}

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}
	

	public boolean equals(Object o) {
		if (this == o) return true;
		if (!o instanceof Dog) return false;
		Dog other = o;
		return this.breed.equals(other.getBreed()) 
			&& this.getAge() == other.getAge();
	}


	public int hashCode() {

	}

}