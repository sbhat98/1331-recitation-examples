public class Dog implements Comparable<Dog> {
	private int age;
	private String name;

	public Dog(int age, String name) {
		this.age = age;
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name + " " + age;
	}

	@Override
	public int compareTo(Dog d) {
		return this.name.compareTo(d.name);
	}
}