// import java.util.Comparator;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class Recitation11 {

	int outerField = 5;


	private class InnerClass {
		//constructors, methods, fields, etc
		int field;

		public InnerClass() {
			this.field = outerField;
			// the above gets the value from the outer class
		}


	}

	// private static class AgeComp implements Comparator<Dog> {
	// 	@Override
	// 	public int compare(Dog d1, Dog d2) {
	// 		return d1.getAge() - d2.getAge();
	// 	}
	// }

	public static void main(String[] args) {
		List<Dog> list = new ArrayList<>();
		list.add(new Dog(1, "puppy"));
		list.add(new Dog(0, "newborn"));
		list.add(new Dog(5, "dog"));

		// Comparator<Dog> ageComp = new AgeComp();

		// Comparator<Dog> ageComp = new Comparator<Dog>() {
		// 	@Override
		// 	public int compare(Dog d1, Dog d2) {
		// 		return d1.getAge() - d2.getAge();
		// 	}
		// }

		// Comparator<Dog> ageComp = (Dog d1, Dog d2) -> {
		// 	return d1.getAge() - d2.getAge();
		// }

		// Comparator<Dog> ageComp = (Dog d1, Dog d2) -> d1.getAge() - d2.getAge();

		// Comparator<Dog> ageComp = (d1, d2) -> d1.getAge() - d2.getAge();

		System.out.println(list);

		Collections.sort(list, (d1, d2) -> d1.getAge() - d2.getAge());
		Collections.sort(list, (d1, d2) -> d1.getName().compareTo(d2.getName()));

		System.out.println(list);

		Collections.sort(list);

		for (int x: new Range(0, 20)) {
			System.out.println(x);
		}
	}

}