import java.util.Iterator;

public class Range implements Iterable<Integer> {
	private int beg;
	private int end;

	public Range(int begInd, int endInd) {
		this.beg = begInd;
		this.end = endInd;
	}

	@Override 
	public Iterator<Integer> iterator() {
		return new Iterator<>() {
			@Override
			public boolean hasNext() {
				return beg < end;
			}

			@Override
			public Integer next() {
				return beg++;
			}
		};
	}
}