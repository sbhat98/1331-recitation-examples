public abstract class Animal implements SoundMaking {
	
	protected int weight;
	protected int height;

	public String sound() {
		return "animal sounds";
	}

	public Animal(int weight, int height) {
		this.weight = weight;
		this.height = height;
	}


	@Override
	public boolean equals(Object o) {
		if (o == this) {return true;}
		if (!(o instanceof Animal)) {return false;}
		Animal a = (Animal) o;
		return this.weight == a.weight && this.height == a.height;
	}
}

class Dog extends Animal {

	@Ov
	public String sound() {
		return "woof";
	}

	public int weight;
	
	public Dog() {
		super(4, 30);
	}
}