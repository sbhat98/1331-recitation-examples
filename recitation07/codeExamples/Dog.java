public class Dog extends Animal implements CanBark {
	// since this class is a subclass of Animal, we don't have to
	// declare the fields "species", "weight", or "numLegs"

	private String breed;
	private String hairColor;
	// we can declare additional fields just for this class, though

	public Dog(String breed, String hairColor, int weight) {
		super("Dog", weight, 4);
		this.breed = breed;
		this.hairColor = hairColor;
		// through this combination of constructor chaining and field
		// declaration, we can take advantage of the fields from the superclass
		// and the fields we've declared just for this class
	}

	@Override
	public void eat() {
		System.out.println("Eating in a manner similar to a dog!");
		// by using the *optional* override tag, the compiler will warn us if
		// we're attempting to incorrectly override a method from a super class
	}

	@Override
	public void bark() {
		System.out.println("Bark!");
	}



	// we inherited the getter and setter methods for the other fields from the superclass
	// so we only have to declare them for the new fields we created
	public String getBreed() {
		return this.breed;
	}

	protected void setBreed(String newBreed) {
		this.breed = newBreed;
	}

	public String getHairColor() {
		return this.hairColor;
	}

	protected void setHairColor(String newHairColor) {
		this.hairColor = newHairColor;
	}

}