public interface CanBark {
	
	public void bark();
	// we only declare the method header here, no implementation
	// this is so that we know that if a class implements this interface,
	// it will have this method in some capacity.
}