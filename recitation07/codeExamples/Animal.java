public abstract class Animal {

	// since this class is abstract, we can create subclasses of it,
	// but can't instantiate objects of its type on its own

	private String species;
	private int weight;
	private int numLegs;
	// all subclasses will have these fields

	public Animal(String species, int weight, int numLegs) {
		// we can't use this constructor since the class is abstract,
		// but we can declare it here to use it in subclasses through
		// constructor chaining

		this.species = species;
		this.weight = weight;
		this.numLegs = numLegs;
	}

	public void eat() {
		System.out.println("Eating food!");
		// since this is something all animals do, we can declare it here
		// and every subtype will have it automatically
	}

	protected void setSpecies(String newSpecies) {
		// due to the "protected" modifier, this method is available
		// to all subclasses
		this.species = newSpecies;
	}

	public String getSpecies(){
		return this.species; 
	}

	protected void setWeight(int newWeight) {
		this.weight = newWeight;
	}

	public int getWeight() {
		return weight;
	}

	protected void setNumLegs(int newNumLegs) {
		this.numLegs = newNumLegs;
	}

}