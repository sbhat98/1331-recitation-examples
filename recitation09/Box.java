import java.util.List;
import java.util.ArrayList;

public class Box<T extends List> {

	List<T> list;

	public Box(List<T> list) {
		this.list = list;
	}

	public void add(T t) {
		list.add(t);
	}

	@Override
	public String toString() {
		return list.toString();
	}

}