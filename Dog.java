public class Dog {

    private String name; // = null;
    private String color; //  null;
    private int age;//= 0;
    private String breed;

    private static int doggieCount;

    public static String sound() {
        return "Woof";
    }

    public String getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Dog(String name, String c, int a, String b) {
        this.name = name;
        this.color = c;
        this.age = a;
        this.breed = b;

        doggieCount++;
    }

    public Dog() {
        this("Bob", "Red", -2, "dog");
    }


}